var routerApp =angular.module('DuoDiginRt', ['ngMaterial','uiMicrokernel',"highcharts-ng",'angular.css.injector' ,
    'ui.router',  
    '720kb.socialshare',
    'FBAngular',
    'configuration'
    ])
 
   routerApp.config(["$locationProvider","$httpProvider","$stateProvider", function($locationProvider,$httpProvider,$stateProvider) {
          $httpProvider.defaults.headers.post['Content-Type'] = 'multipart/form-data';
     $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $stateProvider
        .state('Dashboards', {
         url: "/Dashboards",
         controller  : 'DashboardCtrl',
         templateUrl: "views/charts.html"
         })
         .state('Reports', {
         url: "/Reports",
        controller  : 'ReportCtrl',
         templateUrl: "views/reports.html"
         }) 
         .state('Analytics', {
         url: "/Analytics",
        controller  : 'analyticsCtrl',
         templateUrl: "views/analytics.html"
         })   
         .state('RealTime', {
         url: "/RealTime",
        controller  : 'RealTimeController',
         templateUrl: "views/realtime.html"
         })
         
         .state('Digin Extended', {
         url: "/Digin Extended",
        controller  : 'ExtendedanalyticsCtrl',
         templateUrl: "views/extended-analytics.html"
         })
         .state('Interactive Report',{
         url: "/Interactive Report",
        controller  : 'ExtendedReportCtrl',
         templateUrl: "views/extended-reports.html"
         })
         .state('Analysis Report',{
         url: "/Analysis Report",
        controller  : 'ExtendedanalyticsCtrl',
         templateUrl: "views/extended-analytics.html"
         })
         .state('Dashboard',{
         url: "/Dashboard",
        controller  : 'ExtendedDashboardCtrl',
         templateUrl: "views/extended-dashboard.html"
         })
         .state('PivotTable', {
         url: "/PivotTable",
        controller  : 'summarizeCtrl',
         templateUrl: "views/pivottable.html"
         })             
        }]);

 
  
 routerApp.controller('NavCtrl', ['$scope','$mdBottomSheet','$mdSidenav',
'$timeout','$rootScope','$mdDialog','$objectstore','$state','Fullscreen',
  function($scope,$mdBottomSheet,$mdSidenav, $timeout,$rootScope,$mdDialog,$objectstore,$state,Fullscreen){
 $rootScope.indexes = [];
  $scope.toggle = true;
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();
  if(dd<10) {
    dd='0'+dd
  } 
  if(mm<10) {
    mm='0'+mm
  } 
    $rootScope.dashboard= {
    '1': {
       widgets: [
      ]
    }
  };
  
  
   $scope.ExistingreportDetails=[];
  $scope.GetReportDetails=function(){
  $http({method: 'GET', url: 'http://104.236.192.147:8080/pentaho/api/repo/files/%3Ahome%3Aadmin/children?showHidden=false&filter=*',
           // cache: $templateCache,
           headers:{'Access-Control-Allow-Origin': '*',
           'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With','Authorization': 'Basic YWRtaW46cGFzc3dvcmQ='}
    }).
                    success(function(data, status) {
                      alert(JSON.stringify(data));
					  $scope.ExistingreportDetails=data;
					   //alert("success");
                    }).
                    error(function(data, status) {
                      alert("Request failed");
                      
                  });
    
  
  
  };  
  
  
  
  $scope.ExistingDashboardDetails=[];
   
   $scope.GetDshboardDetails=function(){
   var client = $objectstore.getClient("com.duosoftware.com","duodigin_dashboard");
client.onGetMany(function(data){
      if (data){
		//alert("success");
        //$rootScope.ExistingDashboardDetails =data;
        $scope.ExistingDashboardDetails =data;
     }
     }); 
     client.getByFiltering("*");
   }

   var client = $objectstore.getClient("com.duosoftware.com");
   client.onGetMany(function(data){
   data.forEach(function(entry) {
   $rootScope.indexes.push({ value: entry, display: entry });
  });    
  });       
  client.getClasses("com.duosoftware.com");      

  $scope.closeDialog = function() {           
            $mdDialog.hide();
        };
  today = mm+'/'+dd+'/'+yyyy;
  $rootScope.dashboard.dashboardName = "";
  $rootScope.dashboard.dashboardDate=today;
  $rootScope.dashboard.dashboardType ="System";
  $rootScope.dashboard.dashboardCulture = "English";
  $rootScope.indexes =[];
  $scope.currentView = "";
  $scope.ChartType = 'column';
  $scope.count = 0;
  $scope.incremenet= 0;
  $scope.leftPosition = 110;
  $scope.topPosition = 60;
  $scope.chartSeries = [];
  $scope.dashboard= [];
  $scope.dashboard.widgets =     $rootScope.dashboard["1"].widgets;

        
         $scope.menuPanels = [DashboardCtrl];
          function DashboardCtrl($scope){
            
             $scope.dashboardmenu = [
              {            
              title: 'Widget'           
              },
              {        
               title: 'Save'           
              },
              {        
               title: 'Load'           
              }
              ];

               $scope.Extendedmenu = [
              {            
              title: 'Analysis Report'           
              },
              {        
               title: 'Interactive Report'           
              },
              {        
               title: 'Dashboard'           
              },
               {        
               title: 'Datasource'           
              }
              ];
               $scope.doFunction = function(name){
                   if(name=="Widget")
                   {
                    
                     var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
                      $mdDialog.show({     
                      controller: 'WidgetCtrl', 
                      templateUrl: 'views/newWidget.html',
                      resolve: {
                      dashboard: function() {
                      return $scope.dashboard;
                      }
                    }
                   }) 
                   }
                   if(name == "Analysis Report")
                   {
                    var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
                      $state.go('Analysis Report');   

                   }
                    if(name=="Save")
                   {
                     var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
                    $scope.saveDashboard();
                   }
                     if(name=="Load")
                   {
                      var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
                    $scope.openDashboard();
                   }
                      if(name=="Data summary")
                   {
                     var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
                      $state.go('PivotTable');         
                   }

                   if(name=="New Analytics")
                   {
                      var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
                       $state.go('Analytics');                      
                   }    

                    if(name=="RealTime Extended")
                   {
                     var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
                       $state.go('RealTime');                      
                   }   

                                                      
                   if(name=="Interactive Report")
                   {
                     var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
                     $state.go('Interactive Report');                      
                   }      
              };
             

               $scope.reportmenu = [
              {    

              title: 'Design report'           
              },
              {        
               title: 'View Report'           
              }
              
              ];

                   $scope.analyticsmenu = [
              {            
              title: 'New Analytics'           
              },
              {        
               title: 'Data summary'           
              } 
              ];

                $scope.realtimeMenu = [
              {            
              title: 'Default widgets'           
              },
              {        
               title: 'RealTime Extended'           
              }
              ];

               
            }

           
     $scope.saveDashboard = function(ev,dashboard) {
     $mdDialog.show({
      controller: 'saveCtrl',
      templateUrl: 'views/dashboard-save.html',
      targetEvent: ev,
      resolve: {
          widget: function() {
            return dashboard;
          }
        }
    })
   }

  //    $scope.openSummarize = function(ev) {
  //     $mdDialog.show(
  //     {     
  //     templateUrl: 'views/summarize-data.html',
  //      controller: 'summarizeCtrl'
  //          })
    
  // };





   $scope.Share = function(ev){
      $mdDialog.show({
            controller: 'shareCtrl',
      templateUrl: 'views/dashboard-share.html',
        resolve: {
          
        }
    }) 


    }
    $scope.Export = function(ev)
    {
       $mdDialog.show({
            controller: 'ExportCtrl',
      templateUrl: 'views/chart_export.html',
        resolve: {
          
        }

    })




    }
   $scope.openTheme = function(ev) {
       $mdDialog.show({
            controller: 'ThemeCtrl',
      templateUrl: 'views/change-theme.html',
        resolve: {
          
        }
    });
    
  };
   $scope.openDashboard = function(ev,dashboard) {
       $mdDialog.show({
      controller: 'DataCtrl',
      templateUrl: 'views/dashboard-load.html',
      targetEvent: ev,
      resolve: {
          dashboard: function() {
            return dashboard;
          }
        }
    })
 }
          $scope.navigate = function(routeName){
            if(routeName == "Dashboards")
            {
                var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'block';
               $scope.currentView = "Dashboard";
            $(".menu-layer").css("top", "0px");
            $("starting-point").css("top", "0px");
            
             $state.go(routeName)
           }
           if(routeName == "Reports")
           {
              var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'block';
            $scope.currentView = "Reports";
             $(".menu-layer").css("top", "40px");
              $("starting-point").css("top", "40px");
                
             
               $state.go(routeName)
           }
           if(routeName == "Analytics")
           {
              var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'block';
             $(".menu-layer").css("top", "80px");
              $("starting-point").css("top", "80px");
            $scope.currentView = "Analytics";
           }
            if(routeName == "RealTime")
           {
              var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'block';
             $scope.currentView = "RealTime";
            $(".menu-layer").css("top", "120px");
             $("starting-point").css("top", "120px");
             
               $state.go(routeName)
           }
             if(routeName == "Digin Extended")
           {
              var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'block';
                      $(".menu-layer").css("top", "180px");
              $("starting-point").css("top", "180px");
            
             $scope.currentView = "Digin Extended";
             $state.go(routeName)
           }
            if(routeName == "Theme")
           {
              var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
                $scope.openTheme();
            
           }
             if(routeName == "Share")
           {
              var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
           
                $scope.Share();
            
           }
             if(routeName == "Export")
           {
              var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
           
                $scope.Export();
            
           }
           if(routeName == "Help")
           {

                $mdDialog.show({
            controller: 'HelpCtrl',
      templateUrl: 'Views/help.html',
        resolve: {
          
        }
      });
           }
              if(routeName == "TV Mode")
           {
                 var selectedMenu =  document.getElementsByClassName("menu-layer") ;
                     selectedMenu[0].style.display= 'none';
           
            if (Fullscreen.isEnabled())
            Fullscreen.cancel();
            else
              Fullscreen.all();
            
           }
 
        };


        var icons = ['dashboard','dashboard'];
        var colors = ['#323232', '#262428'];
        $scope.cnt = Math.floor(Math.random() * icons.length);
        $scope.icon = icons[$scope.cnt];
        $scope.fill = colors[0];
        $scope.size = 48;

        setInterval(function() {
            var random = Math.random();
            if (random < 0.2) {
                $scope.size = 28 + 4 * Math.floor(Math.random() * 9);
            } else {
                $scope.cnt++;
                if ($scope.cnt >= icons.length)
                    $scope.cnt = 0;
                $scope.icon = icons[$scope.cnt];
                $scope.fill = colors[Math.floor(Math.random() * colors.length)];
            }
            $scope.$apply();
        }, 1700);
   $scope.menu = [
    {
       title: 'Dashboards',
      icon: 'bower_components/material-design-icons/action/svg/production/ic_dashboard_24px.svg'
    },
    {
      
      title: 'Reports',
      icon: 'bower_components/material-design-icons/device/svg/production/ic_dvr_24px.svg'
    },
    {
     
      title: 'Analytics',
      icon: 'bower_components/material-design-icons/device/svg/production/ic_nfc_24px.svg'
    },
    {
      
      title: 'RealTime',
      icon: 'bower_components/material-design-icons/action/svg/production/ic_explore_24px.svg'
    },
    {
      
      title: 'Digin Extended',
      icon: 'bower_components/material-design-icons/action/svg/production/ic_extension_24px.svg'
    },
    {
      
      title: 'Theme',
      icon: 'bower_components/material-design-icons/editor/svg/production/ic_format_color_fill_24px.svg'
    } ,
     {
      
      title: 'Share',
      icon: 'bower_components/material-design-icons/social/svg/production/ic_share_24px.svg'
    } ,
     {
      
      title: 'Export',
      icon: 'bower_components/material-design-icons/file/svg/production/ic_file_download_24px.svg'
    } ,
     {
      
      title: 'TV Mode',
      icon: 'bower_components/material-design-icons/hardware/svg/production/ic_tv_24px.svg'
    } ,
     {
      
      title: 'Help',
      icon: 'bower_components/material-design-icons/action/svg/production/ic_help_24px.svg'
    } 
  ];
 }
 ]); 
 
 routerApp.run(['$rootScope', function($rootScope){
   
 


}]);
  
 
  routerApp.controller('mainController',['$scope','$http',function(scope,http){
  }]);
 