
routerApp.service('RealTimeService', ['$q', function($q) {
  var analytics = [{
      name: 'RealTime',
      iconurl: 'bower_components/material-design-icons/device/svg/production/ic_dvr_24px.svg',
      imgurl: 'http://192.168.1.194:5601/',
      content: ' '
  } 


   ];

  // Promise-based API
  return {
      loadAll: function() {
    
          return $q.when(analytics);
      }
  };
}]);