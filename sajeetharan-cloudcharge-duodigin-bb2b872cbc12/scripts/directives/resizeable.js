routerApp.directive('resizeable', function() { 
  return { 
    restrict: 'A', 

    link: function(scope, element, attrs) { 
      
      $(element).resizable({ 
        resize: function(event, ui) {     
         
          var dataJ =  JSON.parse(attrs.identifier);
          var  widgetId = dataJ.id;     
            var  widgetPlugin = dataJ.d3plugin;  
             
            if(widgetPlugin != "")
            {
           var myEl   = $("#widgetframe");
          height = ui.size.height ;
         width = ui.size.width ;
         
        
           myEl.css('width', width);
               myEl.css('height', height);
           jsPlumb.repaint(ui.helper); 
        }
        
        else
        {


           var index = $('#'+widgetId).data('highchartsChart');
          var chart=Highcharts.charts[index];        
          height = ui.size.height - 100;
         width = ui.size.width - 40;
         
          chart.setSize(width, height, doAnimation = true);
          
           jsPlumb.repaint(ui.helper); 
        }
        }
        , 
        handles: "all" 

      }); 
    } 
  }; 
});
