

routerApp.controller('saveCtrl', ['$scope', '$http','$objectstore','$mdDialog','$rootScope', 

function ($scope, $http,$objectstore,$mdDialog,$rootScope) {   
 
       $scope.closeDialog = function() {
            // Easily hides most recent dialog shown...
           // no specific instance reference is needed.
            $mdDialog.hide();
        };
      $scope.dashboardName ="";
      $scope.dashboardType = $rootScope.dashboard.dashboardType;
      $scope.dashboardCulture = $rootScope.dashboard.dashboardCulture;
      $scope.dashboardDate = $rootScope.dashboard.dashboardDate;

      $scope.saveDashboardDetails = function(){
      $rootScope.dashboard.dashboardName = $scope.dashboardName;
       
      var client = $objectstore.getClient("com.duosoftware.com","duodigin_dashboard");
      client.onComplete(function(data){ 
           $mdDialog.hide();
            $mdDialog.show({
            controller: 'successCtrl',  
      templateUrl: 'views/dialog_success.html',
        resolve: {
          
        }
    })
      });
      client.onError(function(data){
            $mdDialog.hide();
            $mdDialog.show({
            controller: 'errorCtrl',
      templateUrl: 'views/dialog_error.html',
        resolve: {
          
        }
    })
      });
      
  
      client.insert([$rootScope.dashboard], {KeyProperty:"dashboardName"});           
     
             
      }

 
}]);