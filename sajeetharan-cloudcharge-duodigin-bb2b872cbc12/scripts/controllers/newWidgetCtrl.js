 routerApp.controller('WidgetCtrl', ['$scope', '$timeout', '$rootScope','$mdDialog','$sce','$objectstore','dashboard',
    function($scope, $timeout, $rootScope, $mdDialog,$sce,$objectstore,dashboard) {
  $scope.dashboard = dashboard;
   $scope.leftPosition = 110;
  $scope.topPosition = 60;
  $scope.closeDialog = function() {    
    
    };
 $scope.filterWidgets = function(item)
  {
    if(item == null)
    {
      item.title = "Chart";
    }
    $scope.selected = {};
    $scope.selected.type = item.title; 
    $rootScope.widgetType = $scope.selected.type;
  };
  $scope.WidgetTypes = [
    {
       title: 'Chart',
      icon: 'styles/css/images/icons/ic_insert_chart_24px.svg'
    },
    {
      
      title: 'Blogging',
      icon: 'styles/css/images/icons/ic_assignment_24px.svg'
    },
    {
     
      title: 'Advertising',
      icon: 'styles/css/images/icons/ic_av_timer_24px.svg'
    },
    {
      
      title: 'Custom',
      icon: 'styles/css/images/icons/ic_dashboard_24px.svg'
    },
    {
      
      title: 'Social Media',
      icon: 'styles/css/images/icons/ic_now_widgets_24px.svg'
    } ,
     {
      
      title: 'Web Analytics',
      icon: 'styles/css/images/icons/ic_gradient_24px.svg'
    }  
  ];
      $scope.Widgets = [
    {
       title: 'Google SpreadSheets',
      icon: 'styles/css/images/icons/ic_insert_chart_24px.svg',
       type:"Chart",
       Description:'Line, Area, Bar, Pie, Table'
    },
    {
      
      title: 'Image',
      icon: 'styles/css/images/icons/ic_assignment_24px.svg',
       type:"Custom",
         Description:'Image'
    },
    {
     
      title: 'ElasticSearch',
      icon: 'styles/css/images/icons/ic_av_timer_24px.svg',
       type:"Chart",
         Description:'Line, Area, Bar, Pie, Table'

    },
    {
      
      title: 'CSV File',
      icon: 'styles/css/images/icons/ic_dashboard_24px.svg',
       type:"Chart",
         Description:'Line, Area, Bar, Pie, Table'

    }, 
    {
      
      title: 'WordPress',
      icon: 'styles/css/images/icons/ic_dashboard_24px.svg',
       type:"Blogging",
         Description:'Users, Posts, Comments'
    }, 
     {
      
      title: 'Google AdSense',
      icon: 'styles/css/images/icons/ic_dashboard_24px.svg',
       type:"Advertising",
         Description:'Earnings, Pageviews, Clicks'
    }, 
    {
      
      title: 'Facebook Pages',
      icon: 'styles/css/images/icons/ic_dashboard_24px.svg',
       type:"Social Media",
         Description:'Likes, Clicks, Active Users, Pageviews'
    } ,
    {
      
      title: 'Google+',
      icon: 'styles/css/images/icons/ic_dashboard_24px.svg',
       type:"Social Media",
         Description:'Plus Ones, Circled By'
    } ,
    {
      
      title: 'LinkedIn',
      icon: 'styles/css/images/icons/ic_dashboard_24px.svg',
       type:"Social Media",
         Description:'Connections, Network Updates'
    }, 
    {
      
      title: 'Pinterest',
      icon: 'styles/css/images/icons/ic_dashboard_24px.svg',
       type:"Social Media",
         Description:'Boards, Pins, Likes, Followers, Following'
    },
    {
      
      title: 'Google Analytics (Real Time)',
      icon: 'styles/css/images/icons/ic_dashboard_24px.svg',
       type:"Web Analytics",
         Description:'Audience, Traffic, Content, Conversions'
    }


  ];

   $scope.addAllinOne = function(widgetType) {
    console.log('type of selected widget:'+widgetType);
      $mdDialog.hide();
    if($scope.dashboard.widgets.length == 1)
    {
       $scope.leftPosition = 410;
       $scope.topPosition = 60;
       $scope.ChartType = 'bar';
    }
    if($scope.dashboard.widgets.length ==2)
    {
        $scope.leftPosition = 100;
        $scope.topPosition = 520;
        $scope.ChartType = 'column';
    }
    if($scope.dashboard.widgets.length == 3)
    {
      $scope.leftPosition =  710;
      $scope.topPosition = 520;
      $scope.ChartType = 'pie';
    }
    if($scope.dashboard.widgets.length > 3)
    {
      $scope.leftPosition = 100;
      $scope.topPosition = 50;
      $scope.ChartType = 'spline';

    }

    //save the type of the widget for the purpose of the socialMediaCtrl
    $rootScope.widgetType = widgetType;

  $scope.dashboard.widgets.push(    
  {
    expanded: true,
    seriesname :"",
    dataname:"",
    d3plugin:"",
     divider: false,    
    chartSeries : $scope.chartSeries,
    query:"select * from testJay where Name!= 'Beast Master'",
    id: "chart" + Math.floor(Math.random()*(100-10+1)+10),
    type:"All",
    width : '350px',
    left :  $scope.leftPosition +'px',
    top :  $scope.topPosition +'px',
    height : '360px',
    chartTypes:[
  {"id": "line", "title": "Line"},
  {"id": "spline", "title": "Smooth line"},
  {"id": "area", "title": "Area"},
  {"id": "areaspline", "title": "Smooth area"},
  {"id": "column", "title": "Column"},
  {"id": "bar", "title": "Bar"},
  {"id": "pie", "title": "Pie"},
  {"id": "scatter", "title": "Scatter"}
  ],
       dashStyles:[
  {"id": "Solid", "title": "Solid"},
  {"id": "ShortDash", "title": "ShortDash"},
  {"id": "ShortDot", "title": "ShortDot"},
  {"id": "ShortDashDot", "title": "ShortDashDot"},
  {"id": "ShortDashDotDot", "title": "ShortDashDotDot"},
  {"id": "Dot", "title": "Dot"},
  {"id": "Dash", "title": "Dash"},
  {"id": "LongDash", "title": "LongDash"},
  {"id": "DashDot", "title": "DashDot"},
  {"id": "LongDashDot", "title": "LongDashDot"},
  {"id": "LongDashDotDot", "title": "LongDashDotDot"}
  ],

    chartStack : [
  {"id": '', "title": "No"},
  {"id": "normal", "title": "Normal"},
  {"id": "percent", "title": "Percent"}
  ],
    chartConfig : {
       exporting: {
         enabled: false
},
    options: {
      chart: {         
        type:  $scope.ChartType
      },
     
      plotOptions: {
        series: {
          stacking: ''
        }
      }
    },
   series: $scope.chartSeries,
    title: {
    text: 'Duodigin Widget',
    style: {
        display: 'none'
    }
    },
    subtitle: {
    text: '',
    style: {
        display: 'none'
    }
},
    credits: {
      enabled: false,
    
    },

    loading: false,
    size: {
       height :300,
     width :300
    }
  }
    
  });
  };
 

 
    }
    ]);