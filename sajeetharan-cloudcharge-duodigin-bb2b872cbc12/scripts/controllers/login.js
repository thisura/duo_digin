var app = angular.module("diginLogin", ['ngMaterial','ngAnimate']);

app.controller("LoginCtrl",['$scope','$http','$templateCache','$rootScope','$location','$window', function ($scope,$http,$templateCache,$rootScope,$location,$window) {

 $scope.errorDiv = true;
 $scope.errorMsg = '';	
 $scope.currentUser = '';  
 $scope.password = '';

 

	 $scope.login = function(){
        $rootScope.currentUser = $scope.currentUser;   
         localStorage.setItem("LoginName",$scope.currentUser); 

         	if($scope.currentUser=='')
         	{
         		$scope.errorDiv = false;
         		$scope.errorMsg = 'username cannot be empty';
         	}
         	else if($scope.password=='')
         	{
         		$scope.errorDiv = false;
         		$scope.errorMsg = 'password cannot be empty';
         	}
         	else if($scope.currentUser == 'admin' && $scope.password == 'password')
         	{
                $http.defaults.headers.put = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With'
        };
        $http.defaults.useXDomain = true;

                

         		$scope.errorDiv = true;
         		//window.location = "home.html"  ;
         	}
         	else
         	{
         		$scope.errorDiv = false;
         		$scope.errorMsg = 'username or password is incorrect';
         	}
                           
 }


 $scope.login1 = function(){

    //var urlPentaho = 'http://104.236.192.147:8080/pentaho/api/repo/files/%3Ahome%3Aadmin/children?showHidden=false&filter=*';
    $http({method: 'GET', url: 'http://104.236.192.147:8080/pentaho/api/repo/files/%3Ahome%3Aadmin/children?showHidden=false&filter=*',
           // cache: $templateCache,
           headers:{'Access-Control-Allow-Origin': '*','Authorization': 'Basic YWRtaW46cGFzc3dvcmQ='}
    }).
                    success(function(data, status) {
                      alert(JSON.stringify(data));
                    }).
                    error(function(data, status) {
                      alert("Request failed");
                      
                  });
 };


      $scope.go = function(path) {
    $location.path("/Home");
  };
     
 $scope.enter = function(keyEvent) {
      if (keyEvent.which === 13)
      {
      $scope.login();
    }
    }
}]);

app.config(function($mdThemingProvider,$httpProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('indigo')
    .accentPalette('orange');
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});