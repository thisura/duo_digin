routerApp.controller('SocialMediaCtrl', function($rootScope,$scope,$timeout,$mdDialog) {


$scope.buttonshow = true;
$scope.viewshow = false;


    $scope.linkedInState;
    $scope.widgetType = $rootScope.widgetType;
    $scope.fbLoginResponse = {
        status: '',
        accessToken: ''
    };

    $scope.accounts = [];

    $scope.connectBtnLabel = 'Add Account';
    authenticate($scope);

    $scope.analytics = {
        'Twitter': [
            'Following',
            'Followers',
            'Tweets'
        ],
        'Facebook': [
            'People engaged',
            'Likes',
            'Clicks',
            'Views'
        ],
        'LinkedIn': [
            'Overview'
        ]
    };

    $scope.$watch('type', function(newval, oldval) {
        if (newval) {
            $scope.level2 = $scope.analytics[newval];
        } else {
            $scope.level2 = [];
        }

        // delete the dependent selection, if the master changes
        if (newval !== oldval) {
            $scope.metrics = null;
        }
    });



    /* Facebook pages start */
    


    $scope.addAccount = function(){
        if($scope.widgetType === 'Facebook Pages')
        {
            $scope.fbLogin();
        }
        else if($scope.widgetType === 'LinkedIn')
        {
            $scope.linkedinLogin();
        }
    };

    $scope.fbLogin = function() {
        $timeout(function(){
            if ($scope.fbLoginResponse.status != 'connected') {
                FB.login(function(response) {
                    authenticate($scope);
                }, {
                    scope: 'manage_pages'
                });
            } else {
                FB.logout(function(response) {
                    authenticate($scope);
                    $scope.accounts = [];
                    $scope.userAccountName = '';
                });
            }
        });        
    };

    $scope.linkedinLogin = function(){
        $timeout(function(){
            if (!$scope.linkedInState) {
                IN.User.authorize(function(){
                    console.log('logged into linkedIn');
                    authenticate($scope);
                });
            } else {
                IN.User.logout(function(){                    
                    $scope.accounts = [];
                    $scope.userAccountName = '';
                    console.log('logged out from linkedIn');
                    authenticate($scope);
                });
            }
        });      
    };

    $scope.changePage = function(){
        $timeout(function(){
            FB.api(
                "/"+$scope.fbPageModel,
                function (response) {
                  if (response && !response.error) {
                    console.log(response.likes);

                        $rootScope.pageResponse = response.likes;
                    $rootScope.pageView = response.checkins;
                    $rootScope.pagenewlike = response.new_like_count;
                    $rootScope.pageengage = response.talking_about_count;
                    $rootScope.pagewerehere = response.were_here_count;


                  }
                }
            );
        });
    };


    $scope.getdetail = function(){
 


   $scope.fblikes = $rootScope.pageResponse;
   $scope.fbviews =  $rootScope.pageView;
   $scope.fbnewlike = $rootScope.pagenewlike;
   $scope.fbengage = $rootScope.pageengage;
   $scope.fbwerehere = $rootScope.pagewerehere;
   

$scope.buttonshow = false;
$scope.viewshow = true;


 
  
    console.log($scope.fbviews);
};

     $scope.remove = function() {
  $mdDialog.hide();
  };



    /* Facebook pages end */

});
routerApp.controller('ThemeCtrl', function($scope, $mdBottomSheet, $mdDialog, cssInjector) {

    $scope.applyIndigoCSS = function() {
        cssInjector.removeAll();
        cssInjector.add("styles/css/style1.css");

    }

    $scope.applyMinimalCSS = function() {
        cssInjector.removeAll();
        cssInjector.add("styles/css/style3.css");

    }
    $scope.closeDialog = function() {
        // Easily hides most recent dialog shown...
        // no specific instance reference is needed.
        $mdDialog.hide();
    };
    $scope.applyVioletCSS = function() {
        cssInjector.removeAll();
        cssInjector.add("styles/css/style.css");

    }

});

routerApp.controller('HelpCtrl', function($scope, $mdBottomSheet, $mdDialog, cssInjector) {

    $scope.closeDialog = function() {
        // Easily hides most recent dialog shown...
        // no specific instance reference is needed.
        $mdDialog.hide();
    };

});

function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().    
}

function authenticate(scope){
    console.log('test:'+scope.widgetType);
    scope.connectBtnLabel = 'Add Account';

    if(scope.widgetType === 'Facebook Pages'){

                FB.getLoginStatus(function(response) {

                console.log('fb login response:' + JSON.stringify(response));
                scope.fbLoginResponse.status = response.status;

                
                if (response.status === 'connected') {

                    scope.connectBtnLabel = 'Remove Account';
                    scope.fbLoginResponse.accessToken = response.authResponse.accessToken;
                    console.log('FB login response:' + JSON.stringify(scope.fbLoginResponse));
                    FB.api('/me', function(response) {
                        console.log('Successful login for: ' + JSON.stringify(response));
                        scope.accounts.push(response.name);
                        scope.userAccountName = response.name;
                        /* make the API call */
                        FB.api(
                            "/" + response.id + "/accounts",
                            function(response) {
                                scope.fbPages = [];
                                console.log('page response:' + JSON.stringify(response));
                                if (response && !response.error) {
                                    for (i = 0; i < response.data.length; i++) {
                                        var tempFbPg = {
                                            id: '',
                                            accessToken: '',
                                            name: '',
                                            category: ''
                                        };
                                        tempFbPg.id = response.data[i].id;
                                        tempFbPg.accessToken = response.data[i].access_token;
                                        tempFbPg.name = response.data[i].name;
                                        tempFbPg.category = response.data[i].category;
                                        scope.fbPages.push(tempFbPg);
                                    }
                                    console.log('fbPages array:' + JSON.stringify(scope.fbPages));
                                }
                            }
                        );
                    });

                } else if (response.status === 'not_authorized') {
                    //todo...
                } else {
                    //todo...
                }
            });

    }
    else if(scope.widgetType==='LinkedIn'){
        scope.linkedInState = IN.User.isAuthorized();
        console.log('linkedin login status:'+scope.linkedInState);
        if(scope.linkedInState){
            var url = '/people/~:(id,first-name,last-name,num-connections,picture-url,headline)?format=json';
            IN.API.Raw().url(url).method('GET').body().result(function(res){
                console.log(JSON.stringify(res));
                var username = res.firstName + ' ' + res.lastName;
                scope.accounts.push(username);
                scope.userAccountName = username;
            });
            scope.connectBtnLabel = 'Remove Account';            
        }
        else{

        }
    }
};